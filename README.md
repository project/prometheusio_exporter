# Prometheus.io Exporter


## Contents

 * Description
 * Requirements
 * Installation
 * Configuration
 * Maintainers


## Description

Exports Drupal metrics to be consumed by Prometheus scraper.

The main module provides the following metrics:
  - PHP info.
  - Node count: total and with bundle labels.
  - Node revision count: total and with bundle labels.
  - Extensions: list of modules/themes/profiles installed with name and version
    labels.
  - Queue size: list of queues with number of items in them.
  - User count: total, with status (active/blocked) and role labels.

A set of submodules can be installed to provide additional metrics:
  - `prometheusio_exporter_cache`: cache total hits, miss with bin labels.
    Tag invalidations, with tag and request path labels.
  - `prometheusio_exporter_config`: information whether the drupal config is
    out of sync or not.
  - `prometheusio_exporter_database`: histograms for time spent on select
    queries, with database target name and route labels.
  - `prometheusio_exporter_requests`: histograms for time spent on requests,
    with http method, route name and http code status labels.
  - `prometheusio_exporter_update`: info about existing core/module/theme
    updates, with labels detailing the installed version, the latest version,
    and whether a security release is available for each module.
  - `prometheusio_exporter_comment`: comments count, with status labels.

An example of the exported data can be seen in the
[EXAMPLE_OUTPUT.txt](https://git.drupalcode.org/project/prometheusio_exporter/-/blob/8.x-1.x/EXAMPLE_OUTPUT.txt)
file.

Some more technical information about the architecture of the module can be
found in the
[ARCHITECTURE.md](https://git.drupalcode.org/project/prometheusio_exporter/-/blob/8.x-1.x/ARCHITECTURE.md)
file.

This module was born as a fork of
[prometheus_exporter](http://drupal.org/project/prometheus_exporter) but using
the unofficial third-party library
[PromPHP/prometheus_client_php](https://github.com/PromPHP/prometheus_client_php).

### Security Warning

<strong>WARNING</strong>: this module can expose sensitive information such as module versions
which could be used to identify vulnerabilities. You should ensure access is
only granted to trusted users via Basic Authentication or OAuth2, or protect
with a web application firewall, or apache htaccess rules.

The "Prometheus Exporter Token Access" sub-module can be used to allow access via
a token query string parameter.



## Installation

 * Install with composer, `composer req drupal/prometheusio_exporter`.

Due to change in the underlying version of Symfony between Drupal 9 and Drupal 10, Version 1.x is compatible with Drupal 8.9.x up to 9.x. Version 2.x is compatible with Drupal 10 only.

## Configuration

Configure the Enabled collectors and Metrics Collector order for the
module at 'Configuration > System > Prometheus Exporter' (`/admin/config/system/prometheusio_exporter`).

## Maintainers

Current maintainers:
 * Giuseppe Rota [grota](https://www.drupal.org/u/grota)
