# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

## [1.0.0] - 2022-12-26
- More D10 + drush 11 compatibility fixes.
- Module is still not compatible with D10 due to changes in `Symfony\Component\HttpKernel\HttpKernelInterface` that affect `Drupal\prometheusio_exporter_database\StackMiddleware\DatabaseLogging`, a new major version is required to provide compatibility with D10.
- Require pcb ^3

## [1.0-rc8] - 2022-06-22
- Update requirement `promphp/prometheus_client_php` to "v2.6.1"
- Some phpqa fixes
- D10 patch from https://www.drupal.org/project/prometheusio_exporter/issues/3289169#comment-14569995
- Support only core >= 9.1
- Add support for prometheus summaries.

## [1.0-rc7] - 2021-10-02
- back to using `"promphp/prometheus_client_php": "v2.3.0"` since same metric with different label sets are not supported by client libs in general.
- cache "per tag" metrics now have metric name `cache_total_tag_invalidations_per_tag`
- cache "per tag and request" metrics now have metric name `cache_total_tag_invalidations_per_tag_and_request`
- comment "per status" metrics now have metric name `drupal_comment_count_total_per_status`
- node "per bundle" metrics now have metric name `drupal_node_count_total_per_bundle`
- user "per status" metrics now have metric name `drupal_user_count_total_per_status`
- user "per role" metrics now have metric name `drupal_user_count_total_per_role`

## [1.0-rc6] - 2021-09-30

### Changed

- do not enforce use of permanent cache backend during installation process

## [1.0-rc5] - 2021-09-23

### Changed

- Composer dep for the prometheus library is now the fork `"sparkfabrik/prometheus_client_php":"2.2.3"` since composer does not respect pinned hash for dev branches (https://getcomposer.org/doc/04-schema.md#package-links) nor does load composer repos recursively (https://getcomposer.org/doc/faqs/why-can%27t-composer-load-repositories-recursively.md)

## [1.0-rc4] - 2021-09-17

### Changed

- Node count metrics bypassing `node_access` for performance reasons (/metrics in hit by anon).

## [1.0-rc3] - 2021-09-16
### Added

- A bunch of explicit dev deps to make both drupalci odd pipelines work and local `composer run tests`.
- Checks to prevent installing this module when webprofiler is present.
  Webprofiler changes without discrimination the cache backend classes even is
  a bin`->`backend association is forced in the settings.php.
  Webprofiler's cache backend class does not have pcb's `deleteAllPermanent` method.

## [1.0-rc2] - 2021-08-20

### Added

- Metrics collector are grouped into two categories, plugin and non-plugin, see ARCHITECTURE.md.
- Added extensions metrics (plugin collector).
- Added config metrics (plugin collector).
- Added cache metrics (non plugin collector).
- Added database metrics (non plugin collector).
- Added requests metrics (non plugin collector).
- It's now possible (configurable) to wipe prometheus client storage on config import.
- The database and requests collectors have the buckets of the histograms of
  theirs metrics configurable.
  The prometheus client storage for those collectors is always cleaned if the
  buckets configuration is changed. This is because the underlying library does
  not register histograms with buckets different than the ones it already has
  in storage.
- Removed support for Drupal core 8.8.
- Dependency on drupal/pcb in order not to have our cache bin clean during
  cache clear.
- Support to run `composer run tests` or `composer run phpqa`.
- ARCHITECTURE.md, CHANGELOG.md, more info in README.md.

### Changed

- base module has now a general config page and a plugins config page.
- "update" plugin now outputs these labels:
  `'name', 'version', 'latest_version', 'has_security_update'`.
- `"user_count"` plugin now outputs these labels: role=$roleid, status=active/blocked.

## [1.0-rc1] - 2021-07-07

### Added
- First release of the module
  Forked from `drupal/prometheus_exporter#f50db0478b6d8f85ba81ef7e0b5d7ffc7dcc25f3`
  changing the machine name to `prometheusio_exporter`.
  Changed underlying library to `promphp/prometheus_client_php` which is stateful,
  with a patch (see composer.json) that enables usage of metrics with same name
  but different labels.
