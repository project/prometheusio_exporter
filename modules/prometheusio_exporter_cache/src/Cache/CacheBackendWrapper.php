<?php

namespace Drupal\prometheusio_exporter_cache\Cache;

use Drupal\Core\Cache\Cache;
use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Cache\CacheTagsInvalidatorInterface;
use Drupal\prometheusio_exporter\Bridge\PrometheusBridgeInterface;
use Drupal\prometheusio_exporter\BaseMetricsSourceInterface;

/**
 * Wraps an existing cache backend to track calls to the cache backend.
 */
class CacheBackendWrapper implements CacheBackendInterface, CacheTagsInvalidatorInterface, BaseMetricsSourceInterface {

  /**
   * The wrapped cache backend.
   *
   * @var \Drupal\Core\Cache\CacheBackendInterface
   */
  protected CacheBackendInterface $cacheBackend;

  /**
   * The name of the wrapped cache bin.
   *
   * @var string
   */
  protected string $bin;

  /**
   * The promphp bridge.
   *
   * @var \Drupal\prometheusio_exporter\Bridge\PrometheusBridgeInterface
   */
  protected PrometheusBridgeInterface $promBridge;

  /**
   * Constructs a new CacheBackendWrapper.
   *
   * @param \Drupal\Core\Cache\CacheBackendInterface $cacheBackend
   *   The wrapped cache backend.
   * @param string $bin
   *   The name of the wrapped cache bin.
   * @param \Drupal\prometheusio_exporter\Bridge\PrometheusBridgeInterface $promBridge
   *   The promphp bridge.
   */
  public function __construct(
    CacheBackendInterface $cacheBackend,
    string $bin,
    PrometheusBridgeInterface $promBridge
  ) {
    $this->cacheBackend = $cacheBackend;
    $this->bin = $bin;
    $this->promBridge = $promBridge;
  }

  /**
   * {@inheritdoc}
   */
  public static function getMetricsSourceId(): string {
    return 'cache_hits_and_miss';
  }

  /**
   * {@inheritdoc}
   */
  public function get($cid, $allow_invalid = FALSE) {
    $cache = $this->cacheBackend->get($cid, $allow_invalid);
    if ($cache) {
      $this->registerPrometheusCache('hits');
    }
    else {
      $this->registerPrometheusCache('miss');
    }
    return $cache;
  }

  /**
   * {@inheritdoc}
   */
  public function getMultiple(&$cids, $allow_invalid = FALSE) {
    $cidsCopy = $cids;
    $cache = $this->cacheBackend->getMultiple($cids, $allow_invalid);

    foreach ($cidsCopy as $cid) {
      if (in_array($cid, $cids)) {
        $this->registerPrometheusCache('miss');
      }
      else {
        $this->registerPrometheusCache('hits');
      }
    }

    return $cache;
  }

  /**
   * Register a hit/miss in prometheus, total per bin.
   */
  protected function registerPrometheusCache(string $type) {
    $namespace_name_help = [
      'drupal',
      "cache_total_$type",
      "Total number of cache $type.",
    ];
    $this->promBridge->getCounter(...$namespace_name_help, ...[['bin'], $this])->inc([$this->bin]);
  }

  /**
   * {@inheritdoc}
   */
  public function set($cid, $data, $expire = Cache::PERMANENT, array $tags = []) {
    return $this->cacheBackend->set($cid, $data, $expire, $tags);
  }

  /**
   * {@inheritdoc}
   */
  public function setMultiple(array $items) {
    return $this->cacheBackend->setMultiple($items);
  }

  /**
   * {@inheritdoc}
   */
  public function delete($cid) {
    return $this->cacheBackend->delete($cid);
  }

  /**
   * {@inheritdoc}
   */
  public function deleteMultiple(array $cids) {
    return $this->cacheBackend->deleteMultiple($cids);
  }

  /**
   * {@inheritdoc}
   */
  public function deleteAll() {
    return $this->cacheBackend->deleteAll();
  }

  /**
   * {@inheritdoc}
   */
  public function invalidate($cid) {
    return $this->cacheBackend->invalidate($cid);
  }

  /**
   * {@inheritdoc}
   */
  public function invalidateMultiple(array $cids) {
    return $this->cacheBackend->invalidateMultiple($cids);
  }

  /**
   * {@inheritdoc}
   */
  public function invalidateTags(array $tags) {
    if ($this->cacheBackend instanceof CacheTagsInvalidatorInterface) {
      $this->cacheBackend->invalidateTags($tags);
    }
  }

  /**
   * {@inheritdoc}
   */
  public function invalidateAll() {
    return $this->cacheBackend->invalidateAll();
  }

  /**
   * {@inheritdoc}
   */
  public function garbageCollection() {
    return $this->cacheBackend->garbageCollection();
  }

  /**
   * {@inheritdoc}
   */
  public function removeBin() {
    return $this->cacheBackend->removeBin();
  }

}
