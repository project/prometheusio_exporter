<?php

namespace Drupal\Tests\prometheusio_exporter_update\Unit\Plugin\MetricsCollector;

use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\prometheusio_exporter_update\Plugin\MetricsCollector\UpdateStatusCollector;
use Drupal\update\UpdateManagerInterface;
use Drupal\Tests\prometheusio_exporter\Unit\Plugin\MetricsCollector\AbstractTestBaseMetrics;

/**
 * @coversDefaultClass \Drupal\prometheusio_exporter_update\Plugin\MetricsCollector\UpdateStatusCollector
 * @group prometheusio_exporter
 */
class UpdateStatusCollectorTest extends AbstractTestBaseMetrics {

  use \Prophecy\PhpUnit\ProphecyTrait;

  /**
   * @covers ::collectMetrics
   */
  public function testCollectMetrics() {

    $updateManager = $this->getProphet()->prophesize(UpdateManagerInterface::class);
    $updateManager->projectStorage('update_project_data')->willReturn($this->getTestProjectData());

    $moduleHandler = $this->getProphet()->prophesize(ModuleHandlerInterface::class);
    $moduleHandler->moduleExists('update')->willReturn(TRUE);

    $collector = new UpdateStatusCollector(['description' => 'Dummy description.'], 'update_status_test', [], $this->prometheusBridge, $updateManager->reveal(), $moduleHandler->reveal());
    $collector->executeMetrics();

    $this->assertEquals(<<<EOD
# HELP drupal_update_status_test_core_version Drupal core version
# TYPE drupal_update_status_test_core_version gauge
drupal_update_status_test_core_version{name="test_core",version="8.6.1",latest_version="9.2",has_security_update="0"} 1
# HELP drupal_update_status_test_module_version Drupal module version
# TYPE drupal_update_status_test_module_version gauge
drupal_update_status_test_module_version{name="test_module",version="8.x-1.3",latest_version="8.x-1.3",has_security_update="1"} 1
# HELP drupal_update_status_test_theme_version Drupal theme version
# TYPE drupal_update_status_test_theme_version gauge
drupal_update_status_test_theme_version{name="test_theme",version="8.x-2.x",latest_version="8.x-2.3",has_security_update="0"} 1
EOD, $this->prometheusBridge->render());
  }

  /**
   * Provides test data for the update manager.
   *
   * @return array
   *   The test project data.
   */
  protected function getTestProjectData() {

    return [
      'test_core' => [
        'project_type' => 'core',
        'info' => [
          'version' => '8.6.1',
        ],
        'latest_version' => '9.2',
      ],
      'test_module' => [
        'project_type' => 'module',
        'info' => [
          'version' => '8.x-1.3',
        ],
        'latest_version' => '8.x-1.3',
        'security updates' => ['something'],
      ],
      'test_theme' => [
        'project_type' => 'theme',
        'info' => [
          'version' => '8.x-2.x',
        ],
        'latest_version' => '8.x-2.3',
      ],
    ];
  }

}
