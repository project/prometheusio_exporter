<?php

namespace Drupal\prometheusio_exporter_update\Plugin\MetricsCollector;

use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\prometheusio_exporter\Plugin\BasePluginMetricsCollector;
use Drupal\update\UpdateManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\prometheusio_exporter\Bridge\PrometheusBridgeInterface;

/**
 * Collects metrics for module status.
 *
 * @MetricsCollector(
 *   id = "update_status",
 *   title = @Translation("Update status"),
 *   description = @Translation("Provides metrics for module update status.")
 * )
 */
class UpdateStatusCollector extends BasePluginMetricsCollector {

  /**
   * The update manager.
   *
   * @var \Drupal\update\UpdateManagerInterface
   */
  protected $updateManager;

  /**
   * The module hander.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected $moduleHandler;

  /**
   * UpdateStatusCollector constructor.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\prometheusio_exporter\Bridge\PrometheusBridgeInterface $promBridge
   *   The promphp bridge.
   * @param \Drupal\update\UpdateManagerInterface $updateManager
   *   The update manager.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $moduleHandler
   *   The module handler.
   */
  final public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    PrometheusBridgeInterface $promBridge,
    UpdateManagerInterface $updateManager,
    ModuleHandlerInterface $moduleHandler
  ) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $promBridge);
    $this->updateManager = $updateManager;
    $this->moduleHandler = $moduleHandler;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('prometheusio_exporter.prometheus_bridge'),
      $container->get('update.manager'),
      $container->get('module_handler')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function executeMetrics() {

    $projects = $this->updateManager->projectStorage('update_project_data');

    foreach ($projects as $name => $project) {
      $project_type = $project['project_type'];
      $version = $project['info']['version'];
      $latest_version = $project['latest_version'] ?? '';
      $has_security_update = (int) !empty($project['security updates']);
      $labels = [
        'name',
        'version',
        'latest_version',
        'has_security_update',
      ];
      $labelValues = [
        $name,
        $version,
        $latest_version,
        $has_security_update,
      ];
      switch ($project_type) {
        case 'core':
          $namespace_name_help_labels = [
            $this->getNamespace(),
            'core_version',
            'Drupal core version',
            $labels,
          ];
          break;

        case 'module':
          $namespace_name_help_labels = [
            $this->getNamespace(),
            'module_version',
            'Drupal module version',
            $labels,
          ];
          break;

        case 'theme':
          $namespace_name_help_labels = [
            $this->getNamespace(),
            'theme_version',
            'Drupal theme version',
            $labels,
          ];
          break;
      }
      if (isset($namespace_name_help_labels)) {
        $this->promBridge->getGauge(...$namespace_name_help_labels)->set(1, $labelValues);
      }
    }
  }

}
