<?php

namespace Drupal\prometheusio_exporter_config\Plugin\MetricsCollector;

use Drupal\Core\State\StateInterface;
use Drupal\prometheusio_exporter\Bridge\PrometheusBridgeInterface;
use Drupal\prometheusio_exporter\Plugin\BasePluginMetricsCollector;
use Drupal\prometheusio_exporter_config\Cron\DrupalConfigStateWorker;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Collects metrics for drupal config sync status.
 *
 * @MetricsCollector(
 *   id = "config_status",
 *   title = @Translation("Drupal Config sync status"),
 *   description = @Translation("Provides metrics for drupal config sync status (whether conf is in diff or not).")
 * )
 */
class DrupalConfigSyncStatusCollector extends BasePluginMetricsCollector {

  /**
   * The state key value store.
   *
   * @var \Drupal\Core\State\StateInterface
   */
  protected $state;

  /**
   * {@inheritdoc}
   */
  final public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    PrometheusBridgeInterface $promBridge,
    StateInterface $state
  ) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $promBridge);
    $this->state = $state;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('prometheusio_exporter.prometheus_bridge'),
      $container->get('state')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function executeMetrics() {
    $hasChanges = $this->state->get(DrupalConfigStateWorker::STATE_KEY_HAS_CHANGES, NULL);
    if (is_null($hasChanges)) {
      return;
    }
    $namespace_name_help = [
      $this->getNamespace(),
      'sync',
      $this->getDescription(),
    ];
    $this->promBridge->getGauge(...$namespace_name_help, ...[['has_changes']])->set(1, [(int) $hasChanges]);
  }

}
