<?php

namespace Drupal\prometheusio_exporter_config\Form;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Datetime\DateFormatterInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\prometheusio_exporter_config\Cron\DrupalConfigStateWorker;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Form to configure interval for drupal config status.
 */
class DrupalConfigSettingsForm extends ConfigFormBase {

  /**
   * The date.formatter service.
   *
   * @var \Drupal\Core\Datetime\DateFormatterInterface
   */
  protected $dateFormatter;

  /**
   * {@inheritdoc}
   */
  final public function __construct(
    ConfigFactoryInterface $config_factory,
    DateFormatterInterface $dateFormatter
  ) {
    parent::__construct($config_factory);
    $this->dateFormatter = $dateFormatter;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('date.formatter')
    );
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [DrupalConfigStateWorker::CONFIG_NAME];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'prometheusio_exporter_drupalconfig_settings';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config($this->getEditableConfigNames()[0]);

    $options = [10800, 21600, 43200, 86400, 604800];
    $form['interval'] = [
      '#type' => 'select',
      '#title' => $this->t('Update via cron every'),
      '#description' => $this->t('Update status of drupal config (differences between the filesystem configuration and database configuration)'),
      '#default_value' => $config->get('interval'),
      '#options' => [0 => $this->t('Never')] + array_map(
        [$this->dateFormatter, 'formatInterval'],
        array_combine($options, $options)
      ),
    ];
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $config = $this->config($this->getEditableConfigNames()[0]);
    $config
      ->set('interval', $form_state->getValue('interval'))
      ->save();
    parent::submitForm($form, $form_state);
  }

}
