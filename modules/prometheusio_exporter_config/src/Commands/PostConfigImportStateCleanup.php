<?php

namespace Drupal\prometheusio_exporter_config\Commands;

use Drupal\prometheusio_exporter_config\Cron\DrupalConfigStateWorker;
use Drush\Commands\DrushCommands;
use Consolidation\AnnotatedCommand\CommandData;
use Drupal\Core\State\StateInterface;

/**
 * A Drush commandfile for Prometheus Exporter.
 */
class PostConfigImportStateCleanup extends DrushCommands {

  /**
   * The state service.
   *
   * @var \Drupal\Core\State\StateInterface
   */
  protected $state;

  /**
   * PostConfigImportStateCleanup constructor.
   */
  public function __construct(StateInterface $state) {
    $this->state = $state;
    parent::__construct();
  }

  /**
   * Hooks after the config:import command to clean state.
   *
   * So that if cron runs frequently the config status is refreshed soon after
   * a config:import (which usually identifies a deploy).
   *
   * @SuppressWarnings(PHPMD.UnusedFormalParameter)
   * @hook post-command config:import
   */
  public function postConfigImport($result, CommandData $commandData) {
    $this->state->delete(DrupalConfigStateWorker::STATE_KEY_HAS_CHANGES);
    $this->state->delete(DrupalConfigStateWorker::STATE_KEY_LAST_CHECK);
    $commandData->output()->writeln('Cleaned up variables in "state" for module prometheusio_exporter_config');
  }

}
