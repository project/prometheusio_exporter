<?php

namespace Drupal\prometheusio_exporter_config\Cron;

use Drupal\Component\Datetime\TimeInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Config\ImportStorageTransformer;
use Drupal\Core\Config\StorageComparer;
use Drupal\Core\Config\StorageInterface;
use Drupal\Core\State\StateInterface;

/**
 * Service called during hook_cron.
 *
 * Service is called during hook_cron and if a large enough interval of time
 * has passed from last time it executes an equivalent of drush config:status.
 */
class DrupalConfigStateWorker {

  const STATE_KEY_LAST_CHECK = 'prometheusio_exporter_config.last_check';
  const STATE_KEY_HAS_CHANGES = 'prometheusio_exporter_config.hasChanges';
  const CONFIG_NAME = 'prometheusio_exporter_config.settings';

  /**
   * The cron configuration.
   *
   * @var \Drupal\Core\Config\Config
   */
  protected $config;

  /**
   * The state key value store.
   *
   * @var \Drupal\Core\State\StateInterface
   */
  protected $state;

  /**
   * The datetime.time service.
   *
   * @var \Drupal\Component\Datetime\TimeInterface
   */
  protected TimeInterface $time;

  /**
   * The config.storage service.
   *
   * @var \Drupal\Core\Config\StorageInterface
   */
  protected $configStorage;

  /**
   * The config.storage.sync service.
   *
   * @var \Drupal\Core\Config\StorageInterface
   */
  protected $configStorageSync;

  /**
   * The config.import_transformer service.
   *
   * @var \Drupal\Core\Config\ImportStorageTransformer
   */
  protected $configImportTransformer;

  /**
   * {@inheritdoc}
   */
  public function __construct(
    TimeInterface $time,
    ConfigFactoryInterface $config_factory,
    StateInterface $state,
    StorageInterface $configStorage,
    StorageInterface $configStorageSync,
    ImportStorageTransformer $configImportTransformer
  ) {
    $this->config = $config_factory->get(static::CONFIG_NAME);
    $this->state = $state;
    $this->time = $time;
    $this->configStorage = $configStorage;
    $this->configStorageSync = $configStorageSync;
    $this->configImportTransformer = $configImportTransformer;
  }

  /**
   * Called by hook_cron.
   */
  public function refreshConfigStatusIfIntervalHasPassed() {
    $interval = $this->config->get('interval');
    if (!$interval) {
      return;
    }
    $request_time = $this->time->getRequestTime();
    $last_check = $this->state->get(static::STATE_KEY_LAST_CHECK, 0);
    if (($request_time - $last_check) > $interval) {
      $this->refreshConfigStatus();
      $this->state->set(static::STATE_KEY_LAST_CHECK, $request_time);
    }
  }

  /**
   * Actual refresh.
   */
  protected function refreshConfigStatus() {
    $target_storage = $this->configImportTransformer->transform($this->configStorageSync);
    $config_comparer = new StorageComparer($this->configStorage, $target_storage);
    $hasChanges = $config_comparer->createChangelist()->hasChanges();
    $this->state->set(static::STATE_KEY_HAS_CHANGES, $hasChanges);
  }

}
