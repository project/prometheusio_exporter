<?php

namespace Drupal\prometheusio_exporter_token_access\EventSubscriber;

use Drupal\Core\Routing\RouteSubscriberBase;
use Symfony\Component\Routing\RouteCollection;

/**
 * Alter the metrics route to add our own access control.
 */
class RouteAlterSubscriber extends RouteSubscriberBase {

  /**
   * {@inheritdoc}
   */
  protected function alterRoutes(RouteCollection $collection) {
    if ($route = $collection->get('prometheusio_exporter.metrics')) {
      $route->setRequirements([
        '_prometheus_token_access' => 'true',
      ]);
    }
  }

}
