<?php

namespace Drupal\prometheusio_exporter_database\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\prometheusio_exporter\Traits\FormWithBuckets;
use Drupal\prometheusio_exporter_database\EventSubscriber\DatabaseCollector;

/**
 * Form to configure settings of prometheusio_exporter_database.
 */
class DatabaseSettingsForm extends ConfigFormBase {

  use FormWithBuckets;

  const DATABASE_BUCKETS_MS = [
    0.1,
    0.5,
    1.0,
    5.0,
    10.0,
    30.0,
    50.0,
    150.0,
    300.0,
    500.0,
    1000.0,
    2000.0,
    3000.0,
    5000.0,
    10000.0,
  ];

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [DatabaseCollector::CONFIG_NAME];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'prometheusio_exporter_database_settings';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config($this->getEditableConfigNames()[0]);
    $form['exclude_admin_paths'] = $this->getExcludeAdminPathsFormElement($config->get('exclude_admin_paths') ?? TRUE);
    $form['buckets'] = $this->getBucketsFormElement(
      'database',
      'milliseconds',
      DatabaseCollector::class,
      static::DATABASE_BUCKETS_MS,
      $config->get('buckets') ?? []
    );
    return parent::buildForm($form, $form_state);
  }

  /**
   * Validates that the buckets are float and in increasing order.
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    parent::validateForm($form, $form_state);
    $this->validateBucketsValue($form_state, 'buckets');
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $config = $this->config($this->getEditableConfigNames()[0]);
    $config
      ->set('buckets', $this->parseEolSeparatedValue($form_state->getValue('buckets') ?? ''))
      ->set('exclude_admin_paths', $form_state->getValue('exclude_admin_paths'))
      ->save();
    // We need to clean data in storage to be able to change the buckets.
    $this->promBridge->removeMetricsOfSource(DatabaseCollector::class);
    parent::submitForm($form, $form_state);
  }

}
