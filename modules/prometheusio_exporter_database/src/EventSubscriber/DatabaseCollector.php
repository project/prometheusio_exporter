<?php

namespace Drupal\prometheusio_exporter_database\EventSubscriber;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Database\Database;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\prometheusio_exporter\BaseMetricsSourceInterface;
use Drupal\prometheusio_exporter\Bridge\PrometheusBridgeInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpKernel\Event\TerminateEvent;
use Symfony\Component\HttpKernel\KernelEvents;
use Drupal\Core\Config\ImmutableConfig;

/**
 * Gather db metrics on request end.
 */
class DatabaseCollector implements EventSubscriberInterface, BaseMetricsSourceInterface {

  const CONFIG_NAME = 'prometheusio_exporter_database.settings';

  /**
   * The promphp bridge.
   *
   * @var \Drupal\prometheusio_exporter\Bridge\PrometheusBridgeInterface
   */
  protected PrometheusBridgeInterface $promBridge;

  /**
   * The current_route_match service.
   *
   * @var \Drupal\Core\Routing\RouteMatchInterface
   */
  protected RouteMatchInterface $routeMatch;

  /**
   * The prometheusio database config object.
   *
   * @var \Drupal\Core\Config\ImmutableConfig
   */
  protected ImmutableConfig $config;

  /**
   * {@inheritdoc}
   */
  public function __construct(
    PrometheusBridgeInterface $promBridge,
    RouteMatchInterface $routeMatch,
    ConfigFactoryInterface $config
  ) {
    $this->promBridge = $promBridge;
    $this->routeMatch = $routeMatch;
    $this->config = $config->get(static::CONFIG_NAME);
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    return [
      KernelEvents::TERMINATE => ['collectPrometheusMetricsDatabase', 0],
    ];
  }

  /**
   * {@inheritDoc}
   */
  public static function getMetricsSourceId(): string {
    return 'database';
  }

  /**
   * Event callback.
   *
   * Gather the db logs on request end.
   *
   * @SuppressWarnings(PHPMD.UnusedFormalParameter)
   */
  public function collectPrometheusMetricsDatabase(TerminateEvent $event) {
    $routeName = str_replace('.', '_', $this->routeMatch->getRouteName() ?? '');
    $routeObject = $this->routeMatch->getRouteObject();
    if (empty($routeName) || !$routeObject) {
      return;
    }
    $isAdminRoute = $routeObject->getOptions()['_admin_route'] ?? FALSE;
    if ($isAdminRoute && ($this->config->get('exclude_admin_paths'))) {
      return;
    }

    $buckets = $this->config->get('buckets');
    foreach (array_keys(Database::getAllConnectionInfo()) as $database_key) {
      $database = Database::getConnection('default', $database_key);

      if (!$database->getLogger()) {
        continue;
      }
      $queries = $database->getLogger()->get('prometheusio_exporter');
      $totalTimeInMs = $this->getTotalTimeInMs($queries);
      $this->promBridge->getHistogram(
        'drupal',
        'database_time',
        'Timing metrics for database.',
        [
          'database',
          'route',
        ],
        empty($buckets) ? NULL : $buckets,
        $this
      )
        ->observe($totalTimeInMs, [$database_key, $routeName]);
    }
  }

  /**
   * Get total time in ms of the select queries.
   *
   * @return int|float
   *   The total time of the select queries.
   */
  protected function getTotalTimeInMs(array $queries) {
    $totalTimeInUs = 0;
    foreach ($queries as $query) {
      // Consider only SELECTs.
      if (strpos($query['query'], 'SELECT') === FALSE) {
        continue;
      }
      $totalTimeInUs += $query['time'];
    }
    // Queries have microsecond precision.
    return $totalTimeInUs * 1000;
  }

}
