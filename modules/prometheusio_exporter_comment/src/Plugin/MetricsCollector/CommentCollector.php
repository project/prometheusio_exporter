<?php

namespace Drupal\prometheusio_exporter_comment\Plugin\MetricsCollector;

use Drupal\comment\CommentInterface;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\prometheusio_exporter\Plugin\BasePluginMetricsCollector;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\prometheusio_exporter\Bridge\PrometheusBridgeInterface;

/**
 * Collects metrics for module status.
 *
 * @MetricsCollector(
 *   id = "comment_count",
 *   title = @Translation("Comment Count"),
 *   description = @Translation("Provides metrics for comment counts.")
 * )
 */
class CommentCollector extends BasePluginMetricsCollector {

  /**
   * Any comment status.
   */
  const ANY_STATUS = -1;

  /**
   * The comment storage.
   *
   * @var \Drupal\Core\Entity\EntityStorageInterface
   */
  protected EntityStorageInterface $commentStorage;

  /**
   * UpdateStatusCollector constructor.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\prometheusio_exporter\Bridge\PrometheusBridgeInterface $promBridge
   *   The promphp bridge.
   * @param \Drupal\Core\Entity\EntityStorageInterface $comment_storage
   *   The comment storage.
   */
  final public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    PrometheusBridgeInterface $promBridge,
    EntityStorageInterface $comment_storage
  ) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $promBridge);
    $this->commentStorage = $comment_storage;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('prometheusio_exporter.prometheus_bridge'),
      $container->get('entity_type.manager')->getStorage('comment')
    );
  }

  /**
   * Gets the comment count.
   *
   * @param int $status
   *   (optional) The comment status to filter by.
   *
   * @return int
   *   The comment count.
   */
  protected function getCount($status = self::ANY_STATUS) {
    $query = $this->commentStorage->getQuery();
    $query->accessCheck(TRUE);
    if ($status >= 0) {
      $query->condition('status', $status);
    }
    return $query->count()->accessCheck(TRUE)->execute();
  }

  /**
   * {@inheritdoc}
   */
  public function executeMetrics() {
    $namespace_name_help = [
      $this->getNamespace(),
      'total',
      $this->getDescription(),
    ];
    $this->promBridge->getGauge(...$namespace_name_help)->set($this->getCount());
    $namespace_name_help[1] = 'total_per_status';
    $comment_per_status = $this->promBridge->getGauge(...$namespace_name_help, ...[['status']]);
    $comment_per_status->set($this->getCount(CommentInterface::NOT_PUBLISHED), ['not published']);
    $comment_per_status->set($this->getCount(CommentInterface::PUBLISHED), ['published']);
  }

}
