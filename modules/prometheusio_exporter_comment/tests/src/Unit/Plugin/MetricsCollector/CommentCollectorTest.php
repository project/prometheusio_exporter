<?php

namespace Drupal\Tests\prometheusio_exporter_comment\Unit\Plugin\MetricsCollector;

use Drupal\comment\CommentInterface;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Entity\Query\QueryInterface;
use Drupal\prometheusio_exporter_comment\Plugin\MetricsCollector\CommentCollector;
use Drupal\Tests\prometheusio_exporter\Unit\Plugin\MetricsCollector\AbstractTestBaseMetrics;

/**
 * @coversDefaultClass \Drupal\prometheusio_exporter_comment\Plugin\MetricsCollector\CommentCollector
 * @group prometheusio_exporter
 */
class CommentCollectorTest extends AbstractTestBaseMetrics {

  use \Prophecy\PhpUnit\ProphecyTrait;

  /**
   * @covers ::collectMetrics
   */
  public function testCollect() {

    $commentQuery1 = $this->getProphet()->prophesize(QueryInterface::class);
    $commentQuery1->accessCheck(TRUE)->willReturn($commentQuery1);
    $commentQuery1->count()->willReturn($commentQuery1);
    $commentQuery1->execute()->willReturn(24);

    $commentQuery2 = $this->getProphet()->prophesize(QueryInterface::class);
    $commentQuery2->accessCheck(TRUE)->willReturn($commentQuery2);
    $commentQuery2->condition('status', CommentInterface::NOT_PUBLISHED)->willReturn($commentQuery2);
    $commentQuery2->count()->willReturn($commentQuery2);
    $commentQuery2->execute()->willReturn(1);

    $commentQuery3 = $this->getProphet()->prophesize(QueryInterface::class);
    $commentQuery3->accessCheck(TRUE)->willReturn($commentQuery3);
    $commentQuery3->condition('status', CommentInterface::PUBLISHED)->willReturn($commentQuery3);
    $commentQuery3->count()->willReturn($commentQuery3);
    $commentQuery3->execute()->willReturn(23);

    $comment_storage = $this->getProphet()->prophesize(EntityStorageInterface::class);
    $comment_storage->getQuery()->willReturn($commentQuery1, $commentQuery2, $commentQuery3);

    $definition = [
      'provider' => 'node_count',
      'description' => 'Test description',
    ];

    $collector = new CommentCollector([], 'comment_count', $definition, $this->prometheusBridge, $comment_storage->reveal());

    $collector->executeMetrics();
    $this->assertEquals(<<<EOD
# HELP drupal_comment_count_total Test description
# TYPE drupal_comment_count_total gauge
drupal_comment_count_total 24
# HELP drupal_comment_count_total_per_status Test description
# TYPE drupal_comment_count_total_per_status gauge
drupal_comment_count_total_per_status{status="not published"} 1
drupal_comment_count_total_per_status{status="published"} 23
EOD, $this->prometheusBridge->render());
  }

}
