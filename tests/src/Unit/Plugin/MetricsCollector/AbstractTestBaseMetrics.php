<?php

namespace Drupal\Tests\prometheusio_exporter\Unit\Plugin\MetricsCollector;

use Drupal\Tests\UnitTestCase;
use Drupal\prometheusio_exporter\Bridge\PrometheusBridge;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\prometheusio_exporter\MetricsCollectorManager;
use Drupal\prometheusio_exporter\Prometheus\Storage\DrupalCache;
use Drupal\Tests\prometheusio_exporter\Unit\Cache\PermanentMemoryProxyBackend;

/**
 * Common abstract class.
 */
abstract class AbstractTestBaseMetrics extends UnitTestCase {

  use \Prophecy\PhpUnit\ProphecyTrait;

  /**
   * The promPHP bridge.
   *
   * @var \Drupal\prometheusio_exporter\Bridge\PrometheusBridgeInterface
   */
  protected $prometheusBridge;

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    $drupalCache = new DrupalCache(new PermanentMemoryProxyBackend());
    $prometheusBridge = new PrometheusBridge($drupalCache);
    $container = $this->getProphet()->prophesize(ContainerInterface::class);
    $mcm = $this->getProphet()->prophesize(MetricsCollectorManager::class);
    $container->get('prometheusio_exporter.metrics_collector_manager')->willReturn($mcm);
    $prometheusBridge->setContainer($container->reveal());
    $this->prometheusBridge = $prometheusBridge;
  }

}
