<?php

namespace Drupal\Tests\prometheusio_exporter\Unit\Plugin\MetricsCollector;

use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Entity\Query\QueryInterface;
use Drupal\prometheusio_exporter\Plugin\MetricsCollector\NodeCount;

/**
 * @coversDefaultClass \Drupal\prometheusio_exporter\Plugin\MetricsCollector\NodeCount
 * @group prometheusio_exporter
 */
class NodeCountTest extends AbstractTestBaseMetrics {

  use \Prophecy\PhpUnit\ProphecyTrait;

  /**
   * @covers ::collectMetrics
   */
  public function testCollectMetrics() {
    $nodeQuery1 = $this->getProphet()->prophesize(QueryInterface::class);
    $nodeQuery1->accessCheck(FALSE)->willReturn($nodeQuery1);
    $nodeQuery1->count()->willReturn($nodeQuery1);
    $nodeQuery1->execute()->willReturn(12);

    $nodeQuery2 = $this->getProphet()->prophesize(QueryInterface::class);
    $nodeQuery2->accessCheck(FALSE)->willReturn($nodeQuery2);
    $nodeQuery2->condition('type', 'article')->willReturn($nodeQuery2);
    $nodeQuery2->count()->willReturn($nodeQuery2);
    $nodeQuery2->execute()->willReturn(42);

    $nodeTypeStorage = $this->getProphet()->prophesize(EntityStorageInterface::class);
    $nodeTypeStorage->getQuery()->willReturn($nodeQuery1, $nodeQuery2);

    $definition = [
      'provider' => 'node_count',
      'description' => 'Test description',
    ];

    $collector = new NodeCount([], 'node_count', $definition, $this->prometheusBridge, $nodeTypeStorage->reveal());
    $collector->setConfiguration([
      'enabled' => TRUE,
      'weight' => 0,
      'settings' => [
        'bundles' => [
          'article' => 'article',
        ],
      ],
    ]);

    $collector->executeMetrics();
    $this->assertEquals(<<<EOD
 # HELP drupal_node_count_total Test description
 # TYPE drupal_node_count_total gauge
 drupal_node_count_total 12
 # HELP drupal_node_count_total_per_bundle Test description
 # TYPE drupal_node_count_total_per_bundle gauge
 drupal_node_count_total_per_bundle{bundle="article"} 42
 EOD, $this->prometheusBridge->render());
  }

}
