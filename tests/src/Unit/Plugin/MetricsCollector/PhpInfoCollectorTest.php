<?php

namespace Drupal\Tests\prometheusio_exporter\Unit\Plugin\MetricsCollector;

use Drupal\prometheusio_exporter\Plugin\MetricsCollector\PhpInfoCollector;
use Drupal\prometheusio_exporter\Plugin\MetricsCollector\PhpVersion;

/**
 * @coversDefaultClass \Drupal\prometheusio_exporter\Plugin\MetricsCollector\PhpInfoCollector
 * @group prometheusio_exporter
 */
class PhpInfoCollectorTest extends AbstractTestBaseMetrics {

  use \Prophecy\PhpUnit\ProphecyTrait;

  /**
   * @covers ::collectMetrics
   */
  public function testCollectMetrics() {

    $phpVersion = $this->getProphet()->prophesize(PhpVersion::class);
    $phpVersion->getString()->willReturn('7.2.10');
    $phpVersion->getId()->willReturn(70210);
    $phpVersion->getMajor()->willReturn(7);
    $phpVersion->getMinor()->willReturn(2);
    $phpVersion->getPatch()->willReturn(10);

    $collector = new PhpInfoCollector(['description' => 'Dummy description.'], 'phpinfo', [], $this->prometheusBridge, $phpVersion->reveal());

    $collector->executeMetrics();
    $this->assertEquals(<<<EOD
# HELP drupal_phpinfo_version Provides the PHP version
# TYPE drupal_phpinfo_version gauge
drupal_phpinfo_version{version="7.2.10",major="7",minor="2",patch="10"} 70210
EOD, $this->prometheusBridge->render());
  }

}
