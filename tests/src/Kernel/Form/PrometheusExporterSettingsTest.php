<?php

namespace Drupal\Tests\prometheusio_exporter\Kernel\Form;

use Drupal\Tests\prometheusio_exporter\Kernel\PrometheusExporterKernelTestBase;
use Symfony\Component\HttpFoundation\Request;

/**
 * @coversDefaultClass \Drupal\prometheusio_exporter\Form\PrometheusExporterPluginSettings
 * @group prometheusio_exporter
 */
class PrometheusExporterSettingsTest extends PrometheusExporterKernelTestBase {

  /**
   * The user for testing.
   *
   * @var \Drupal\user\UserInterface
   */
  protected $user;

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();
    $this->user = $this->createUser(['administer prometheus exporter settings']);
    $this->setCurrentUser($this->user);
  }

  /**
   * Tests the metrics endpoint.
   */
  public function testSettingsForm() {
    $request = Request::create('/admin/config/system/prometheusio_exporter/plugins-settings');
    $response = $this->httpKernel->handle($request)->getContent();

    $this->assertStringContainsString("Plugins settings", $response);

    // @todo add form assertions.
  }

}
