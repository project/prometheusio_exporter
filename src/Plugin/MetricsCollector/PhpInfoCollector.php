<?php

namespace Drupal\prometheusio_exporter\Plugin\MetricsCollector;

use Drupal\prometheusio_exporter\Plugin\BasePluginMetricsCollector;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\prometheusio_exporter\Bridge\PrometheusBridgeInterface;

/**
 * Collects metrics for php info.
 *
 * @MetricsCollector(
 *   id = "phpinfo",
 *   title = @Translation("PHP Info"),
 *   description = @Translation("Provides metrics for PHP info.")
 * )
 */
class PhpInfoCollector extends BasePluginMetricsCollector {

  /**
   * The PHP version.
   *
   * @var \Drupal\prometheusio_exporter\Plugin\MetricsCollector\PhpVersion
   */
  protected $phpVersion;

  /**
   * PhpInfoCollector constructor.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\prometheusio_exporter\Bridge\PrometheusBridgeInterface $promBridge
   *   The promphp bridge.
   * @param \Drupal\prometheusio_exporter\Plugin\MetricsCollector\PhpVersion $phpVersion
   *   The PHP info.
   */
  final public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    PrometheusBridgeInterface $promBridge,
    PhpVersion $phpVersion
  ) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $promBridge);
    $this->phpVersion = $phpVersion;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('prometheusio_exporter.prometheus_bridge'),
      new PhpVersion()
    );
  }

  /**
   * {@inheritdoc}
   */
  public function executeMetrics() {
    $this->promBridge->getGauge(
      $this->getNamespace(),
      'version',
      'Provides the PHP version',
      [
        'version',
        'major',
        'minor',
        'patch',
      ]
    )
      ->set(
      $this->phpVersion->getId(),
      [
        $this->phpVersion->getString(),
        $this->phpVersion->getMajor(),
        $this->phpVersion->getMinor(),
        $this->phpVersion->getPatch(),
      ]
    );
  }

}
