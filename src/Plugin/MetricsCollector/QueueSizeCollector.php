<?php

namespace Drupal\prometheusio_exporter\Plugin\MetricsCollector;

use Drupal\Core\Queue\QueueFactory;
use Drupal\Core\Queue\QueueWorkerManagerInterface;
use Drupal\prometheusio_exporter\Plugin\BasePluginMetricsCollector;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\prometheusio_exporter\Bridge\PrometheusBridgeInterface;

/**
 * Collects metrics for queue sizes.
 *
 * @MetricsCollector(
 *   id = "queue_size",
 *   title = @Translation("Queue size"),
 *   description = @Translation("Provides metrics for queue sizes.")
 * )
 */
class QueueSizeCollector extends BasePluginMetricsCollector {

  /**
   * The queue factory.
   *
   * @var \Drupal\Core\Queue\QueueFactory
   */
  protected $queueFactory;

  /**
   * The queue worker manager.
   *
   * @var \Drupal\Core\Queue\QueueWorkerManagerInterface
   */
  protected $queueWorkerManager;

  /**
   * UpdateStatusCollector constructor.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\prometheusio_exporter\Bridge\PrometheusBridgeInterface $promBridge
   *   The promphp bridge.
   * @param \Drupal\Core\Queue\QueueFactory $queueFactory
   *   The queue factory.
   * @param \Drupal\Core\Queue\QueueWorkerManagerInterface $queueWorkerManager
   *   The queue worker manager.
   */
  final public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    PrometheusBridgeInterface $promBridge,
    QueueFactory $queueFactory,
    QueueWorkerManagerInterface $queueWorkerManager
  ) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $promBridge);
    $this->queueFactory = $queueFactory;
    $this->queueWorkerManager = $queueWorkerManager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('prometheusio_exporter.prometheus_bridge'),
      $container->get('queue'),
      $container->get('plugin.manager.queue_worker')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function executeMetrics() {
    foreach ($this->queueWorkerManager->getDefinitions() as $queueName => $queue) {
      $queue = $this->queueFactory->get($queueName);
      $size = $queue->numberOfItems();
      $this->promBridge->getGauge(
        $this->getNamespace(),
        'total',
        $this->getDescription(),
        ['queue']
      )->set($size, [$queueName]);
    }
  }

}
