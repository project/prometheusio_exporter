<?php

namespace Drupal\prometheusio_exporter\Plugin\MetricsCollector;

use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\prometheusio_exporter\Plugin\BasePluginMetricsCollector;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\prometheusio_exporter\Bridge\PrometheusBridgeInterface;

/**
 * Collects metrics for the total node count.
 *
 * @MetricsCollector(
 *   id = "user_count",
 *   title = @Translation("User count"),
 *   description = @Translation("Total user count.")
 * )
 */
class UserCount extends BasePluginMetricsCollector {

  /**
   * The user type storage.
   *
   * @var \Drupal\Core\Entity\EntityStorageInterface
   */
  protected $userStorage;

  /**
   * The user_role type storage.
   *
   * @var \Drupal\Core\Entity\EntityStorageInterface
   */
  protected $roleStorage;

  /**
   * UserCount constructor.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\prometheusio_exporter\Bridge\PrometheusBridgeInterface $promBridge
   *   The promphp bridge.
   * @param \Drupal\Core\Entity\EntityStorageInterface $user_storage
   *   The user type storage.
   * @param \Drupal\Core\Entity\EntityStorageInterface $role_storage
   *   The user_role type storage.
   */
  final public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    PrometheusBridgeInterface $promBridge,
    EntityStorageInterface $user_storage,
    EntityStorageInterface $role_storage
  ) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $promBridge);
    $this->userStorage = $user_storage;
    $this->roleStorage = $role_storage;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('prometheusio_exporter.prometheus_bridge'),
      $container->get('entity_type.manager')->getStorage('user'),
      $container->get('entity_type.manager')->getStorage('user_role')
    );
  }

  /**
   * Gets a count for this metric.
   *
   * @param array $conditions
   *   (optional) An array of query conditions.
   *
   * @return int
   *   The user count.
   */
  protected function getUserCount(array $conditions = []) {
    $query = $this->userStorage->getQuery();
    $query->accessCheck(FALSE);
    foreach ($conditions as $condition) {
      $query->condition(...$condition);
    }
    $count = $query->count()->accessCheck(FALSE)->execute();
    return $count;
  }

  /**
   * {@inheritdoc}
   */
  public function executeMetrics() {
    $namespace_name_help = [
      $this->getNamespace(),
      'total',
      $this->getDescription(),
    ];
    $this->promBridge->getGauge(...$namespace_name_help)
      ->set($this->getUserCount());
    $namespace_name_help[1] = 'total_per_status';
    $gaugeTotalPerStatus = $this->promBridge->getGauge(...$namespace_name_help, ...[['status']]);
    $gaugeTotalPerStatus->set($this->getUserCount([['status', TRUE]]), ['active']);
    $gaugeTotalPerStatus->set($this->getUserCount([['status', FALSE]]), ['blocked']);
    $roleNames = array_keys($this->roleStorage->loadMultiple());
    $namespace_name_help[1] = 'total_per_role';
    $gaugeTotalPerRole = $this->promBridge->getGauge(...$namespace_name_help, ...[['role']]);
    foreach ($roleNames as $roleName) {
      $gaugeTotalPerRole->set($this->getUserCount([['roles', $roleName]]), [$roleName]);
    }
    $gaugeTotalPerRole->set($this->getUserCount([['roles', NULL, 'IS NULL']]), ['no_roles']);
  }

}
