<?php

namespace Drupal\prometheusio_exporter\Routing;

use Drupal\Core\Authentication\AuthenticationCollectorInterface;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\prometheusio_exporter\Controller\MetricsController;
use Drupal\Core\Routing\RouteObjectInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Routing\Route;
use Symfony\Component\Routing\RouteCollection;

/**
 * Defines dynamic routes.
 */
class Routes implements ContainerInjectionInterface {

  /**
   * The authentication collector.
   *
   * @var \Drupal\Core\Authentication\AuthenticationCollectorInterface
   */
  protected $authCollector;

  /**
   * List of providers.
   *
   * @var string[]|null
   */
  protected $providerIds;

  /**
   * Instantiates a Routes object.
   *
   * @param \Drupal\Core\Authentication\AuthenticationCollectorInterface $auth_collector
   *   The authentication provider collector.
   */
  final public function __construct(AuthenticationCollectorInterface $auth_collector) {
    $this->authCollector = $auth_collector;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('authentication_collector')
    );
  }

  /**
   * Provides the routes.
   */
  public function routes(): RouteCollection {
    $collection = new RouteCollection();

    $route = new Route('/metrics', [
      RouteObjectInterface::CONTROLLER_NAME => MetricsController::class . '::metrics',
    ]);
    $route->setRequirement('_permission', 'access prometheus metrics');
    $route->setMethods(['GET']);
    $route->addOptions([
      '_auth' => $this->authProviderList(),
    ]);
    $collection->add('prometheusio_exporter.metrics', $route);

    return $collection;
  }

  /**
   * Build a list of authentication provider ids.
   *
   * @return string[]
   *   The list of IDs.
   */
  protected function authProviderList(): array {
    if (isset($this->providerIds)) {
      return $this->providerIds;
    }
    $this->providerIds = array_keys($this->authCollector->getSortedProviders());

    return $this->providerIds;
  }

}
