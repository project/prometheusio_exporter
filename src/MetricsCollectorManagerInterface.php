<?php

namespace Drupal\prometheusio_exporter;

/**
 * Collects metrics for export to prometheus.
 */
interface MetricsCollectorManagerInterface {

  /**
   * Gather metrics for each collector of type plugin.
   */
  public function executeMetricsPlugins();

  /**
   * Syncs the plugin config removing any obsolete config.
   *
   * @see prometheusio_exporter_modules_uninstalled()
   */
  public function syncPluginConfig();

  /**
   * Calculates what the syncd config should be.
   *
   * @param array $config
   *   An associative array of current collector config.
   * @param string[] $plugin_ids
   *   The plugin IDs of newly installed or uninstalled collector plugins.
   *
   * @return array
   *   The updated config.
   */
  public static function calculateSyncCollectorConfig(array $config, array $plugin_ids);

  /**
   * Gets the metrics collector plugins.
   *
   * @return \Drupal\prometheusio_exporter\MetricsCollectorPluginCollection
   *   The plugins.
   */
  public function getPlugins();

}
