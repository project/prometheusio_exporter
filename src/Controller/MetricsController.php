<?php

namespace Drupal\prometheusio_exporter\Controller;

use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Response;
use Drupal\prometheusio_exporter\Bridge\PrometheusBridgeInterface;

/**
 * A controller for exporting prometheus metrics.
 */
class MetricsController extends ControllerBase {

  /**
   * The promPHP bridge.
   *
   * @var \Drupal\prometheusio_exporter\Bridge\PrometheusBridgeInterface
   */
  protected PrometheusBridgeInterface $promBridge;

  /**
   * MetricsController constructor.
   *
   * @param \Drupal\prometheusio_exporter\Bridge\PrometheusBridgeInterface $promBridge
   *   The promPHP bridge.
   */
  final public function __construct(PrometheusBridgeInterface $promBridge) {
    $this->promBridge = $promBridge;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('prometheusio_exporter.prometheus_bridge')
    );
  }

  /**
   * Handles metrics requests.
   */
  public function metrics() {
    $response = new Response();
    $response->setMaxAge(0);
    $response->headers->set('Content-Type', 'text/plain; version=0.0.4');
    $response->setContent($this->promBridge->render());
    return $response;
  }

}
