<?php

namespace Drupal\prometheusio_exporter\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\prometheusio_exporter\MetricsCollectorManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\prometheusio_exporter\MetricsCollectorManager;

/**
 * Provides a form for managing Prometheus Exporter settings.
 */
class PrometheusExporterPluginSettings extends FormBase {

  /**
   * The plugins.
   *
   * @var \Drupal\prometheusio_exporter\MetricsCollectorPluginCollection
   */
  protected $plugins;

  /**
   * PrometheusExporterPluginSettings constructor.
   *
   * @param \Drupal\prometheusio_exporter\MetricsCollectorManagerInterface $metricsManager
   *   The plugin manager.
   */
  final public function __construct(MetricsCollectorManagerInterface $metricsManager) {
    $this->plugins = $metricsManager->getPlugins();
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('prometheusio_exporter.metrics_collector_manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'prometheusio_exporter_plugins_settings';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['#tree'] = TRUE;
    // Check enabled.
    $form['collectors']['enabled'] = [
      '#type' => 'item',
      '#title' => $this->t('Enabled collectors'),
      '#prefix' => '<div id="collectors-enabled-wrapper">',
      '#suffix' => '</div>',
      // This item is used as a pure wrapping container with heading. Ignore its
      // value, since 'collectors' should only contain check definitions.
      // See https://www.drupal.org/node/1829202.
      '#input' => FALSE,
    ];
    // Check order (tabledrag).
    $form['collectors']['order'] = [
      '#type' => 'table',
      '#attributes' => ['id' => 'check-order'],
      '#title' => $this->t('Metrics Collector order'),
      '#tabledrag' => [
        [
          'action' => 'order',
          'relationship' => 'sibling',
          'group' => 'check-order-weight',
        ],
      ],
      '#tree' => FALSE,
      '#input' => FALSE,
      '#theme_wrappers' => ['form_element'],
    ];
    // Check settings.
    $form['collection_settings'] = [
      '#type' => 'vertical_tabs',
      '#title' => $this->t('Metrics Collector settings'),
    ];

    /** @var \Drupal\prometheusio_exporter\Plugin\PluginMetricsCollectorInterface $plugin */
    foreach ($this->plugins as $plugin_id => $plugin) {
      if (!$plugin->applies()) {
        $this->logger('prometheusio_exporter')->info("Skipping plugin $plugin_id");
        continue;
      }
      $form['collectors']['enabled'][$plugin_id] = [
        '#type' => 'checkbox',
        '#title' => $plugin->getLabel(),
        '#default_value' => $plugin->isEnabled(),
        '#parents' => ['collectors', $plugin_id, 'enabled'],
        '#description' => $plugin->getDescription(),
        '#weight' => $plugin->getWeight(),
      ];

      $form['collectors']['order'][$plugin_id]['#attributes']['class'][] = 'draggable';
      $form['collectors']['order'][$plugin_id]['#weight'] = $plugin->getWeight();
      $form['collectors']['order'][$plugin_id]['collection'] = [
        '#markup' => $plugin->getLabel(),
      ];
      $form['collectors']['order'][$plugin_id]['weight'] = [
        '#type' => 'weight',
        '#title' => $this->t('Weight for @title', ['@title' => $plugin->getLabel()]),
        '#title_display' => 'invisible',
        '#delta' => 50,
        '#default_value' => $plugin->getWeight(),
        '#parents' => ['collectors', $plugin_id, 'weight'],
        '#attributes' => ['class' => ['check-order-weight']],
      ];
      // Retrieve the settings form of the plugin.
      $settings_form = [
        '#parents' => ['collectors', $plugin_id, 'settings'],
        '#tree' => TRUE,
      ];
      $settings_form = $plugin->settingsForm($settings_form, $form_state);
      if (!empty($settings_form)) {
        $form['collectors']['settings'][$plugin_id] = [
          '#type' => 'details',
          '#title' => $plugin->getLabel(),
          '#open' => TRUE,
          '#weight' => $plugin->getWeight(),
          '#parents' => ['collectors', $plugin_id, 'settings'],
          '#group' => 'collectors_settings',
          '#states' => [
            'visible' => [
              ':input[name="collectors[' . $plugin_id . '][enabled]"]' => ['checked' => TRUE],
            ],
          ],
        ];
        $form['collectors']['settings'][$plugin_id] += $settings_form;
      }
    }

    $form['actions'] = [
      '#type' => 'actions',
      'submit' => [
        '#type' => 'submit',
        '#value' => $this->t('Save configuration'),
        '#button_type' => 'primary',
      ],
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $config = $this->configFactory()->getEditable(MetricsCollectorManager::CONFIG_NAME);
    $collectors = [];
    foreach ($form_state->getValue('collectors') as $plugin_id => $collector) {
      $plugin = $this->plugins->get($plugin_id);
      $plugin->setConfiguration($collector);
      $collectors[$plugin_id] = $plugin->getConfiguration();
    }
    $config->set('collectors', $collectors)->save();
    $this->messenger()->addStatus($this->t('The configuration options have been saved.'));
  }

}
