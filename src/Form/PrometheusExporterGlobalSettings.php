<?php

namespace Drupal\prometheusio_exporter\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\prometheusio_exporter\MetricsCollectorManager;

/**
 * Provides a form for managing Prometheus Exporter settings.
 */
class PrometheusExporterGlobalSettings extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'prometheusio_exporter_global_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [MetricsCollectorManager::CONFIG_NAME];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config($this->getEditableConfigNames()[0]);
    $form['wipe_client_storage_on_drush_config_import'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Wipe storage after config import'),
      '#description' => $this->t('Wipe prometheus client storage after config import'),
      '#default_value' => $config->get('wipe_client_storage_on_drush_config_import') ?? TRUE,
    ];
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $config = $this->config($this->getEditableConfigNames()[0]);
    $config
      ->set('wipe_client_storage_on_drush_config_import', $form_state->getValue('wipe_client_storage_on_drush_config_import'))
      ->save();
    parent::submitForm($form, $form_state);
  }

}
