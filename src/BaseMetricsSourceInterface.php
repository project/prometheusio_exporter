<?php

namespace Drupal\prometheusio_exporter;

/**
 * Interface for non-plugin collectors.
 *
 * I.e. collectors whose metrics need to be linked to a source id.
 */
interface BaseMetricsSourceInterface {

  /**
   * Returns a string identifying the metrics source.
   */
  public static function getMetricsSourceId(): string;

}
