<?php

namespace Drupal\prometheusio_exporter\Commands;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drush\Commands\DrushCommands;
use Consolidation\AnnotatedCommand\CommandData;
use Drupal\prometheusio_exporter\Bridge\PrometheusBridgeInterface;
use Drupal\prometheusio_exporter\MetricsCollectorManager;
use Drupal\Core\Config\ImmutableConfig;

/**
 * A Drush commandfile for Prometheus Exporter.
 */
class PostConfigImportStorageWipe extends DrushCommands {

  /**
   * The promphp bridge.
   *
   * @var \Drupal\prometheusio_exporter\Bridge\PrometheusBridgeInterface
   */
  protected PrometheusBridgeInterface $promBridge;

  /**
   * The cron configuration.
   *
   * @var \Drupal\Core\Config\ImmutableConfig
   */
  protected ImmutableConfig $drupalConfig;

  /**
   * PostConfigImportStorageWipe constructor.
   */
  public function __construct(
    PrometheusBridgeInterface $promBridge,
    ConfigFactoryInterface $config_factory
  ) {
    $this->promBridge = $promBridge;
    $this->drupalConfig = $config_factory->get(MetricsCollectorManager::CONFIG_NAME);
  }

  /**
   * Hooks after the config:import command to wipe (client) prometheus storage.
   *
   * A config:import usually identifies a deploy, here we wipe the client
   * metrics storage to start fresh.
   *
   * @SuppressWarnings(PHPMD.UnusedFormalParameter)
   * @hook post-command config:import
   */
  public function postConfigImport($result, CommandData $commandData) {
    if ($this->drupalConfig->get('wipe_client_storage_on_drush_config_import') ?? TRUE) {
      $this->promBridge->wipeMetrics();
      $commandData->output()->writeln('Wiped client prometheus storage');
    }
  }

}
