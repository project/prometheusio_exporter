<?php

namespace Drupal\prometheusio_exporter\Commands;

use Drush\Commands\DrushCommands;
use Drupal\prometheusio_exporter\Bridge\PrometheusBridgeInterface;

/**
 * A Drush commandfile for Prometheus Exporter.
 */
class PrometheusExporterCommands extends DrushCommands {

  /**
   * The promPHP bridge.
   *
   * @var \Drupal\prometheusio_exporter\Bridge\PrometheusBridgeInterface
   */
  protected PrometheusBridgeInterface $promBridge;

  /**
   * PrometheusExporterCommands constructor.
   */
  public function __construct(PrometheusBridgeInterface $promBridge) {
    $this->promBridge = $promBridge;
    parent::__construct();
  }

  /**
   * Export prometheus metrics.
   *
   * @usage prometheus:export
   *   Export prometheus metrics.
   *
   * @command prometheus:export
   */
  public function export() {
    $this->io()->write($this->promBridge->render());
  }

}
